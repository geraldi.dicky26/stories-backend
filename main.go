package main

import (
	"stories.id/config"
	"stories.id/router"
)

func main() {
	config.Init()
	e := router.Init()

	e.Logger.Fatal(e.Start(":7980"))
}
