package controllers

import (
	"fmt"

	"github.com/labstack/echo"
)

type TransferRequest struct {
	ToAccount string `json:"to_account_number" validate:"required"`
	Amount    int    `json:"amount" validate:"required"`
}

func (r *TransferRequest) Bind(c echo.Context, fromAccount string) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	if err := c.Validate(r); err != nil {
		return err
	}
	if fromAccount == r.ToAccount {
		return fmt.Errorf("Nomor akun penerima dan pemilik tidak boleh sama")
	}
	return nil
}
