package db

import (
	"stories.id/config"
)

type result struct {
	AccountNumber string `json:"account_number"`
	Name          string `json:"customer_name"`
	Balance       string `json:"balance"`
}

func FindOneAccount(accountNumber string) (result, error) {
	var result result
	database := config.DbManager()

	join := database.Table("accounts").Select("account_number, name, balance").Joins("inner join customers as c on accounts.customer_number = c.customer_number")
	err := join.Where("account_number =  ?", accountNumber).Take(&result).Error

	if err != nil {
		return result, err
	}

	return result, err
}

func UpdateBalance(fromAccount, toAccount result) error {
	database := config.DbManager()
	updateBalanceFrom := database.Table("accounts").Where("account_number = ?", fromAccount.AccountNumber).Update("balance", fromAccount.Balance).Error
	if updateBalanceFrom != nil {
		return updateBalanceFrom
	}

	updateBalanceTo := database.Table("accounts").Where("account_number = ?", toAccount.AccountNumber).Update("balance", toAccount.Balance).Error
	if updateBalanceTo != nil {
		return updateBalanceTo
	}

	return nil
}
