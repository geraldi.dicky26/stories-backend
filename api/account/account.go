package account

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"stories.id/api/account/controllers"
	db "stories.id/api/account/models"
	"stories.id/middleware"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

func checkIsAccountAvail(err error, account string) (int, error) {
	if errors.Is(err, gorm.ErrRecordNotFound) == true {
		return http.StatusNotFound, fmt.Errorf("Akun tidak ditemukan: %s", account)
	} else {
		return http.StatusInternalServerError, err
	}
}

func GetAccount(c echo.Context) error {
	account := c.Param("accountNumber")
	if account == "" {
		return c.JSON(http.StatusUnprocessableEntity, middleware.NewError(fmt.Errorf("Nomor akun harus diisi")))
	}

	data, err := db.FindOneAccount(account)
	code, errnya := checkIsAccountAvail(err, account)
	if errnya != nil {
		return c.JSON(code, middleware.NewError(errnya))
	}

	return c.JSON(http.StatusOK, data)
}

func TransferAccount(c echo.Context) error {
	fromAccount := c.Param("fromAccount")
	if fromAccount == "" {
		return c.JSON(http.StatusUnprocessableEntity, middleware.NewError(fmt.Errorf("Akun tujuan harus terisi")))
	}

	req := &controllers.TransferRequest{}
	if err := req.Bind(c, fromAccount); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, middleware.NewError(err))
	}

	data, err := db.FindOneAccount(fromAccount)
	code, errnya := checkIsAccountAvail(err, fromAccount)
	if errnya != nil {
		return c.JSON(code, middleware.NewError(errnya))
	}

	balance, _ := strconv.Atoi(data.Balance)
	fromAccountBalance := balance - req.Amount

	if fromAccountBalance < 0 {
		return c.JSON(http.StatusBadRequest, middleware.NewError(fmt.Errorf("Saldo kamu tidak cukup, yuk topup dulu..")))
	} else {
		accountTo, errNew := db.FindOneAccount(req.ToAccount)
		code, errnya = checkIsAccountAvail(errNew, req.ToAccount)

		if errnya != nil {
			return c.JSON(code, middleware.NewError(errnya))
		}

		balance, _ := strconv.Atoi(accountTo.Balance)
		toAccountBalance := balance + req.Amount

		data.Balance = strconv.Itoa(fromAccountBalance)
		accountTo.Balance = strconv.Itoa(toAccountBalance)

		err := db.UpdateBalance(data, accountTo)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, middleware.NewError(err))
		}
	}

	return c.JSON(http.StatusCreated, map[string]interface{}{})
}
