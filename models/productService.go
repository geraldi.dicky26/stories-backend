package models

import (
	"github.com/jinzhu/gorm"
)

type Product struct {
	gorm.Model
	ProductId       string `json:"productId"`
	UserShopId      string `json:"userShopId"`
	ProductName     string `json:"productName"`
	Notes           string `json:"notes"`
	Price           int    `json:"price"`
	DetailProduct   string `json:"detailProduct"`
	ProductCategory string `json:"productCategory"`
	ImageUrl        string `json:"imageUrl"`
	Productcolor    string `json:"productColor"`
	Stock           int    `json:"stock"`
}

type ProductDetail struct {
	gorm.Model
	ProductId     string  `json:"productId"`
	IsPromo       string  `json:"isPromo"`
	Rating        float32 `json:"rating"`
	Notes         string  `json:"notes"`
	Price         int     `json:"price"`
	DetailProduct string  `json:"detailProduct"`
}

type ProductReview struct {
	gorm.Model
	ReviewId   string  `json:"reviewId"`
	UserId     string  `json:"userId"`
	ProductId  string  `json:"productId"`
	Review     string  `json:"review"`
	RatingGive float32 `json:"ratingGive"`
	Photo      string  `json:"photo"`
}

type UserWishlist struct {
	gorm.Model
	UserId    string `json:"userId"`
	ProductId string `json:"productId"`
}
