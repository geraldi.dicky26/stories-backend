package models

import (
	"github.com/jinzhu/gorm"
)

type Shipping struct {
	gorm.Model
	ShippingId       string `json:"shippingId"`
	ShippingAddress  string `json:"shippingAddress"`
	ShippingPhoneNum string `json:"shippingPhoneNum"`
	ShippingName     string `json:"shippingName"`
	ShippingNotes    string `json:"shippingNotes"`
}

type Transaction struct {
	gorm.Model
	OrderId       string  `json:"orderId"`
	UserId        string  `json:"userId"`
	SendTo        string  `json:"sendTo"`
	ShippingId    string  `json:"shippingId"`
	GatewayFee    float32 `json:"gatewayFee"`
	PaymentMethod string  `json:"paymentMethod"`
	RewardsPoint  float32 `json:"rewardsPoints"`
	TotalPaid     float32 `json:"totalPaid"`
	Status        string  `json:"status"`
}

type TransactionDetail struct {
	gorm.Model
	OrderId           string  `json:"orderId"`
	ProductId         string  `json:"productId"`
	ShippingMethod    string  `json:"shippingMethod"`
	ShippingPay       string  `json:"shippingPay"`
	Quantity          int     `json:"quantity"`
	TotalPrice        float32 `json:"totalPrice"`
	IsComplain        bool    `json:"isComplain"`
	Reason            string  `json:"reason"`
	HasTestimonial    bool    `json:"hasTestimonial"`
	TestimonialReview string  `json:"testimonialReview"`
	Rating            float32 `json:"rating"`
	TrackingNo        bool    `json:"trackingNo"`
	PromoCode         string  `json:"promoCode"`
}
