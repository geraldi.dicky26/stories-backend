package models

import (
	"github.com/jinzhu/gorm"
)

type UserCoupon struct {
	gorm.Model
	UserId   string `json:"userId"`
	CouponId string `json:"couponId"`
	IsActive bool   `json:"isActive"`
}

type Coupon struct {
	gorm.Model
	CouponId           string `json:"couponId"`
	CouponCode         string `json:"couponCode"`
	CouponName         string `json:"couponName"`
	StartDate          string `json:"startDate"`
	EndDate            string `json:"endDate"`
	PointToPay         int    `json:"pointToPay"`
	IsActive           bool   `json:"isActive"`
	MinimumTransaction string `json:"minimumTransaction"`
	Sk                 string `json:"sk"`
}
