package models

import (
	"github.com/jinzhu/gorm"
)

type UserAuth struct {
	gorm.Model
	UserId   string  `json:"userId"`
	Type     bool    `json:"type"`
	Device   string  `json:"device"`
	Ip       string  `json:"ip"`
	Lon      float32 `json:"lon"`
	Lat      float32 `json:"lat"`
	Version  string  `json:"version"`
	Token    string  `json:"token"`
	IsActive bool    `json:"isActive"`
	TokenUid string  `json:"tokenuid"`
}

type UserActivityLog struct {
	gorm.Model
	UserId string `json:"userId"`
	Action string `json:"action"`
}
