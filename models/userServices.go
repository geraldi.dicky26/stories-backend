package models

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Email         string `json:"email"`
	Msisdn        string `json:"msisdn"`
	Name          string `json:"name"`
	Password      string `json:"password"`
	PhoneVerified bool   `json:"phoneVerified"`
	Gender        bool   `json:"gender"`
	IsMerchant    bool   `json:"isMerchant"`
	BirthDate     string `json:"birthDate"`
}

type UserShipping struct {
	gorm.Model
	ShippingId            string  `json:"shippingId"`
	UserId                string  `json:"userId"`
	Label                 string  `json:"label"`
	UserAddress           string  `json:"userAddress"`
	Lon                   float32 `json:"lon"`
	Lat                   float32 `json:"lat"`
	RecevierName          string  `json:"recevierName"`
	NumberPhone           string  `json:"numberPhone"`
	ZipCode               string  `json:"zipCode"`
	AdditionalInformation string  `json:"additionalInformation"`
}

type UserPoints struct {
	gorm.Model
	UserId string `json:"userId"`
	Points int    `json:"points"`
}

type UserCart struct {
	gorm.Model
	UserId    string `json:"userId"`
	ProductId string `json:"productId"`
}

type UserHistory struct {
	gorm.Model
	UserId    string `json:"userId"`
	ProductId string `json:"productId"`
	Action    string `json:"action"`
}
