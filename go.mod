module stories.id

go 1.13

require (
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/google/uuid v1.2.0 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/jcmturner/gofork v1.0.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo-contrib v0.9.0 // indirect
	github.com/labstack/echo/v4 v4.1.17 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.9.0 // indirect
	github.com/minio/highwayhash v1.0.1
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/crypto v0.0.0-20201203163018-be400aefbc4c // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/jcmturner/dnsutils.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/gokrb5.v7 v7.5.0
)
