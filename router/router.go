package router

import (
	"stories.id/api/account"
	"stories.id/middleware"

	"github.com/labstack/echo"
)

func Init() *echo.Echo {
	e := echo.New()

	middlewares := middleware.InitMiddleware()
	e.Use(middlewares.CORS)
	e.Use(middleware.MiddlewareLogging)
	e.HTTPErrorHandler = middleware.ErrorHandler
	e.Validator = middleware.NewValidator()

	accountRoute := e.Group("/account")

	accountRoute.POST("/registration", account.GetAccount)
	accountRoute.POST("/login", account.TransferAccount)

	return e
}
