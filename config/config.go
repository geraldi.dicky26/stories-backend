package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

// Initializar .env variable
func init() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("No .env file found")
	}
}

// struct of configuration
type Configuration struct {
	DBConfig database
}

// Database struct
type database struct {
	DB_Username string
	DB_Password string
	DB_Host     string
	DB_Driver   string
	DB_Port     string
	DB_Name     string
}

func SetupConfig() Configuration {
	configuration := Configuration{}

	// Database configuration
	configuration.DBConfig.DB_Driver = os.Getenv("DB_DRIVER")
	configuration.DBConfig.DB_Host = os.Getenv("DB_HOST")
	configuration.DBConfig.DB_Username = os.Getenv("DB_USER")
	configuration.DBConfig.DB_Password = os.Getenv("DB_PASSWORD")
	configuration.DBConfig.DB_Port = os.Getenv("DB_PORT")
	configuration.DBConfig.DB_Name = os.Getenv("DB_NAME")

	return configuration
}
