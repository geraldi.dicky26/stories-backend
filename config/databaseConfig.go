package config

import (
	"fmt"
	"log"

	"stories.id/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB
var err error

// setup and create database model
func Init() {
	configuration := SetupConfig()
	connect_string := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", configuration.DBConfig.DB_Host, configuration.DBConfig.DB_Port,
		configuration.DBConfig.DB_Username, configuration.DBConfig.DB_Name, configuration.DBConfig.DB_Password)

	db, err = gorm.Open(configuration.DBConfig.DB_Driver, connect_string)

	if err != nil {
		fmt.Println(err)
		panic("DB Connection Error")
	}

	migrate(db)
}

func DbManager() *gorm.DB {
	return db
}

func migrate(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.Coupon{}, &models.Product{}, &models.ProductDetail{},
		&models.ProductReview{}, &models.Shipping{}, &models.Transaction{}, &models.TransactionDetail{}, &models.User{},
		&models.UserActivityLog{}, &models.UserAuth{}, &models.UserCart{}, &models.UserCoupon{}, &models.UserHistory{},
		&models.UserPoints{}, &models.UserShipping{}, &models.UserWishlist{}).Error
	if err != nil {
		log.Fatalf("Failed drop table: %v", err)
	}
	err = db.Debug().AutoMigrate(&models.Coupon{}, &models.Product{}, &models.ProductDetail{},
		&models.ProductReview{}, &models.Shipping{}, &models.Transaction{}, &models.TransactionDetail{}, &models.User{},
		&models.UserActivityLog{}, &models.UserAuth{}, &models.UserCart{}, &models.UserCoupon{}, &models.UserHistory{},
		&models.UserPoints{}, &models.UserShipping{}, &models.UserWishlist{}).Error
	if err != nil {
		log.Fatalf("Failed migrate table: %v", err)
	}
}
