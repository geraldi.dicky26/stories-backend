package middleware

import (
	"fmt"

	"github.com/labstack/echo"
)

type Error struct {
	Errors map[string]interface{} `json:"errors"`
}

func CustomError(error map[string]interface{}) Error {
	e := Error{}
	e.Errors = error
	fmt.Println(e)
	return e
}

func NewError(err error) Error {
	e := Error{}
	e.Errors = make(map[string]interface{})
	switch v := err.(type) {
	case *echo.HTTPError:
		e.Errors["tipeError"] = v.Code
		e.Errors["message"] = v.Message
	default:
		e.Errors["tipeError"] = "General error"
		e.Errors["message"] = v.Error()
	}
	fmt.Println(err)
	return e
}

func AccessForbidden() Error {
	e := Error{}
	e.Errors = make(map[string]interface{})
	e.Errors["errorType"] = "Access Denied"
	e.Errors["message"] = "access forbidden"
	fmt.Println(e)
	return e
}

func NotFound() Error {
	e := Error{}
	e.Errors = make(map[string]interface{})
	e.Errors["errorType"] = "No resource or invalid url"
	e.Errors["message"] = "resource not found"
	fmt.Println(e)
	return e
}
